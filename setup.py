from setuptools import setup
from pathlib import Path


def read_requirements(name: str):
    p = Path(name).parent.joinpath(name)
    reqs = [line for line in p.read_text().splitlines() if line]
    return reqs

setup(
    install_requires=read_requirements("requirements.txt"),
    entry_points = {
        'console_scripts': ['hello-py=hello_py.main:main'],
    }
)